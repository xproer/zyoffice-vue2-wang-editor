# zyOffice-vue2-cli-wangEditor4
泽优Office文档转换程序（zyOffice） for vue2-cli-wangEditor 4示例

## 介绍
泽优Office文档转换服务（zyOffice）是由荆门泽优软件有限公司开发的一个支持多平台(Windows,macOS,Linux)的简化Word内容发布的组件。适用于政府门户，集约化平台，CMS，OA，博客，文档管理系统，微信公众号，微博，自媒体，传媒，在线教育等领域。主要帮助用户解决Word文档一键导入的问题，能够支持从ie6到chrome的全部浏览器和常用操作系统（Windows,MacOS,Linux）及信创和国产化环境（龙芯，中标麒麟，银河麒麟，统信UOS）。能够大幅度提高企业信息发布效率，帮助企业实现全媒体平台信息一体化战略。

## 环境支持
系统支持：Windows,macOS,Linux,CentOS,中标麒麟,银河麒麟,统信UOS  
CPU支持：x86(海光,兆芯,Intel,AMD),arm(鲲鹏，飞腾),龙芯(mips,LoongArch)  
浏览器支持：IE6,IE7,IE8,IE9,IE10,IE11,火狐Firefox,谷歌Chrome,Edge,苹果Safari,欧朋Opera,奇安信,360安全浏览器,360极速浏览器,龙芯浏览器,猎豹浏览器,搜狗浏览器,红莲花浏览器,QQ浏览器,傲游浏览器,2345浏览器,115浏览器,UC浏览器,世界之窗浏览器,百度浏览器  
开发语言：ASP,ASP.NET,.NET Core,.NET MVC,JAVA,PHP  
前端框架：VUE,React  
编辑器：FCKEditor,CKEditor,TinyMCE,KindEditor,xhEditor,wangEditor

## 安装教程(服务端)

[Windows](http://www.ncmem.com/doc/view.aspx?id=82170058de824b5c86e2e666e5be319c)  
[Linux(deb)](http://www.ncmem.com/doc/view.aspx?id=6b8018d41ee24fa5955788ef72d5eeae)  
[Linux(rpm)](http://www.ncmem.com/doc/view.aspx?id=5bebc255e8eb4a3baac8e312b6e98092)  

## 集成
[fckeditor 2](http://www.ncmem.com/doc/view.aspx?id=039a9d16a99343a5a7b279d6897558c4)  
[ckeditor 3](http://www.ncmem.com/doc/view.aspx?id=673b7999eaeb4478b1426b1134208776)  
[ckeditor 4](http://www.ncmem.com/doc/view.aspx?id=d7c57019fcbb49bfa9993ed23afbd436)  
[ckeditor 5-vue2-cli](http://www.ncmem.com/doc/view.aspx?id=46650d7ccdbb4d6ebf8d9b0427e0566c)  
[kindeditor 3](http://www.ncmem.com/doc/view.aspx?id=58e5e2b05f7f41de8dbff8f0306f2c50)  
[kindeditor 4](http://www.ncmem.com/doc/view.aspx?id=ac50af11f4c24999828abe55b51101bf)  
[wangEditor 3](http://www.ncmem.com/doc/view.aspx?id=039a88ca91cd49dca6ecfed88c2d4a58)  
[wangEditor 4-vue2-cli](http://www.ncmem.com/doc/view.aspx?id=1b59665af5ad4ce0947ef7f6f8e62742)  
[umeditor 1.x](http://www.ncmem.com/doc/view.aspx?id=6f25b387bfc943debb055b8e37dc3c31)  
[ueditor 1.4](http://www.ncmem.com/doc/view.aspx?id=5dcb46be47f74990bfafc905b3ff48ee)  
[ueditor 1.4-vue2-cli](http://www.ncmem.com/doc/view.aspx?id=a93c93b4f99d47f79edd6fb26e2bac57)  
[ueditor 1.5](http://www.ncmem.com/doc/view.aspx?id=b1708bc00053435594bdce0d9992b047)  
[ueditor 1.5-vue2-cli](http://www.ncmem.com/doc/view.aspx?id=ebc5628b0f5d4497b4c3f081a42e0eb6)  
[tinymce 3](http://www.ncmem.com/doc/view.aspx?id=76e1a90b1ba24f69ae73a601491f5b18)  
[tinymce 4](http://www.ncmem.com/doc/view.aspx?id=4ad73cb548ba419eb37a14adc86ce009)  
[tinymce 5](http://www.ncmem.com/doc/view.aspx?id=8be16bc1ba8b4af9b38f33ba26d641f3)  
[tinymce 6](http://www.ncmem.com/doc/view.aspx?id=42a2ac646ffa45eb8a21fb7fdd9c6811)  
[xhEditor](http://www.ncmem.com/doc/view.aspx?id=fb0282f0fca1464d89192537b1bc1918)  
[eWebEditor 9](http://www.ncmem.com/doc/view.aspx?id=cff4e9935ed545c0ab02c3f2ddee929f)  

## 成功案例

北京银联信科技股份有限公司  
优慕课在线教育科技（北京）有限责任公司  
西安工业大学  
西安恒谦教育科技股份有限公司  
西安德雅通科技有限公司  
国家气象中心  
国开泛在（北京）教育科技有限公司  
北京大唐融合通信技术有限公司  
北京思路创新科技有限公司  
北京兴油工程项目管理有限公司  
北京海泰方圆科技股份有限公司

## 联系方式
[QQ群：223813913](http://shang.qq.com/wpa/qunwpa?idkey=24fd50b8d8aad7e7b81504d8ba35eea748cc56efa97f2a15b5b7afe1f4f413fe)  
微信：13235643658  
QQ：1269085759(技术)  
QQ：1085617561(商务)  
邮箱：qwl@ncmem.com